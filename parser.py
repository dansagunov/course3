from rules import *
from dict import parse_dict_word
from collections import namedtuple

Match = namedtuple ('Match', 'rule nonterm text submatches attrs collected')
separators = '?!" ,.;»'

cache = {}

def _parse_text (text, nonterm, rule, pos, vars, match):
    h = hash((
            text,
            -1 if match == None else match.text,
            nonterm, 
            -1 if rule == None else rule.id,
            pos,
            -1 if vars == None else tuple (vars),
            -1 if match == None else tuple (sorted (match.attrs.items ()))
    ))

    if h in cache:
        return cache[h]
    cache[h] = []

    if rule == None and len (nonterm) == 0:
        cache[h] = list (map (lambda attrs: Match (rule=None, nonterm='', text=text, attrs=dict (attrs), submatches=[], collected=[]), parse_dict_word (text)))
        return cache[h]

    if rule != None:
        if pos > len (rule.parts):
            return cache[h]

        if pos == len (rule.parts):
            if len (text) == 0:
                cache[h] = [match]
            return cache[h]

        part = rule.parts[pos]
        if type (part) == str:
            if text.startswith (part):
                cache[h] = _parse_text (text[len (part):], nonterm, rule, pos + 1, vars, match)
            return cache[h]
        else:
            assert (type (part) == Nonterm)
            if pos == len (rule.parts) - 1:
                prefs = [len (text)]
            else:
                npart = rule.parts[pos + 1]
                prefs = []
                if type (npart) == str:
                    pref = 0
                    while pref != -1:
                        pref = text.find (npart, pref + 1)
                        if pref == -1:
                            break
                        prefs.append (pref)
                else:
                    for i in range (len (text)):
                        if text[i] in separators:
                            prefs.append (i)

            result = []
            for pref in reversed (prefs):
                submatches = _parse_text (text[:pref], part.name, None, 0, None, None)
                for submatch in submatches:
                    nmatch = Match (text=match.text, nonterm=nonterm, rule=match.rule, submatches=list (match.submatches), attrs=dict (match.attrs), collected=list (match.collected))
                    nvars = list (vars)
                    good = True

                    for attrname in part.attrs:
                        if attrname in submatch.attrs:
                            val = submatch.attrs[attrname]
                            attr = part.attrs[attrname]
                            if len (attr.value) > 0:
                                if attr.value[0] == '@':
                                    varid = int (attr.value[1:]) - 1
                                    if len (nvars[varid]) == 0 or nvars[varid] == val:
                                        nvars[varid] = val
                                    else:
                                        good = False
                                        break
                                else:
                                    if attr.value != val:
                                        good = False
                                        break
                            if attr.export and not attrname in nmatch.attrs:
                                nmatch.attrs[attrname] = val

                    if good:
                        nmatch.submatches.append (submatch)
                        if 'collect' in part.flags:
                            nmatch = nmatch._replace (collected=nmatch.collected+submatch.collected)
                        result += _parse_text (text[pref:], nonterm, rule, pos + 1, nvars, nmatch)
            cache[h] = result
            return cache[h]
    else:
        assert (len (nonterm) > 0)
        if not nonterm in rules:
            return cache[h]

        result = []
        rem = set ()

        for rule in rules[nonterm]:
            matches = _parse_text (text, nonterm, rule, 0, [''] * rule.cvars, Match (rule=rule, nonterm=nonterm, text=text, submatches=[], attrs=rule.attrs, collected=[]))
            for match in matches:
                ah = hash (tuple (sorted (match.attrs.items ())))
                if ah in rem:
                    continue
                rem.add (ah)
                nmatch = match
                if 'collect' in rule.flags:
                    nmatch = nmatch._replace(collected=[match.text])
                result.append (nmatch)

#        if len (result) > 0:
#            print (text + ' ' + nonterm)

        cache[h] = result
        return cache[h]

def parse_text (text, nonterm='sentence'):
    text = text.lower ()
    return _parse_text (text, nonterm, None, 0, None, None)

import_rules ('rules.yaml')
