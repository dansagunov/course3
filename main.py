from parser import parse_text
import sys
from datetime import datetime

def match_to_str (match):
    if len (match.submatches) == 0:
        return match.text
    result = ""
    pos = 0
    for part in match.rule.parts:
        if type (part) == str:
            result += part
        else:
            result += "(" + match_to_str (match.submatches[pos]) + ")"
            pos += 1
    return result

def print_match (match, depth=0):
    print (' ' * depth + '|'  + match.text + ' ' + match.nonterm + ' ' + str (match.attrs) + str (match.collected))
    for submatch in match.submatches:
        print_match (submatch, depth + 1)
    return

def print_pred_pairs (match):
    if len (match.submatches) == 0:
        return
    subjs = []
    preds = []
    pos = 0
    for part in match.rule.parts:
        if type (part) != str:
            smatch = match.submatches[pos]
            print_pred_pairs (smatch)
            pos += 1
            cnt = 0
            if 'subjects' in part.flags:
                subjs += smatch.collected
            if 'predicates' in part.flags:
                preds += smatch.collected
    if len (subjs) + len (preds) > 0:
        print (str (subjs) + ' ' + str (preds))
    return

def shorten (s):
    if len (s) > 50:
        return s[:49] + '$'
    else:
        return s

succ = 0
index = 0
for line in sys.stdin:
    line = line[:-1]
    index += 1
    print ()
    print ('Parsing sentence "' + shorten (line) + '"...')
    a = datetime.now ()
    matches = parse_text (line)
    if len (matches) == 0:
        print ('Fail')
    else:
        print ('Success!')
        print_pred_pairs (matches[0])
#        print_match (matches[0])
        succ += 1
    print ('Took ' + str (datetime.now () - a))
    print ()
