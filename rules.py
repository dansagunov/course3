import yaml
from collections import namedtuple

rules = {}

Rule = namedtuple ('Rule', 'id cvars text attrs parts flags')
Nonterm = namedtuple ('Nonterm', 'name attrs flags')
Attr = namedtuple ('Attr', 'value export')

def parse_nonterm (pattern):
    if len (pattern) == 0 or pattern[0] != '{':
        return None
    pos = pattern.find ('}')
    if pos == -1:
        return None

    tokens = pattern[1:pos].split (' ')
    name = ""
    attrs = {}
    flags = {}

    for i in range (len (tokens)):
        token = tokens[i]

        if len (token) == 0:
            continue

        values = token.split ('=')
        if len (values) < 1 or len (values[0]) == 0:
            continue

        if values[0][0] == '?':
            flags[values[0][1:]] = 1
            continue

        rvalue = len (values) > 1 and len (values[1]) > 0
        attrname = ""
        attr = Attr (value = "", export = False)
        if values[0][0] == '!':
            attrname = values[0][1:]
            attr = attr._replace (export = True)
        elif i > 0 or rvalue:
            attrname = values[0]
        else:
            name = values[0]

        if rvalue:
            attr = attr._replace (value = values[1])
        
        if attrname != "":
            attrs[attrname] = attr

    return Nonterm(name = name, attrs = attrs, flags=flags), pattern[pos+1:]
       

def parse_term (pattern):
    if len (pattern) == 0:
        return None
    for i in range (len (pattern)):
        if pattern[i] == '{':
            return pattern[0:i], pattern[i:]
        if pattern[i] == '}':
            return None
    return pattern, ""

def parse_rule (pattern, id):
    if len (pattern) == 0:
        return None

    text = pattern
    parts = []
    attrs = {}
    flags = {}
    cvars = 0

    if pattern[0] == '@':
        nonterm = parse_nonterm (pattern[1:])
        if nonterm == None:
            return None
        nt, pattern = nonterm
        for attr in nt.attrs:
            attrs[attr] = nt.attrs[attr].value
        flags = dict (nt.flags)
    
    while len (pattern) > 0:
        if pattern[0] == '{':
            tup = parse_nonterm (pattern)
        else:
            tup = parse_term (pattern)

        if tup == None:
            return None

        part, pattern = tup
        parts.append (part)

    for part in parts:
        if type (part) == Nonterm:
            for an in part.attrs:
                attr = part.attrs[an]
                if len (attr.value) > 0 and attr.value[0] == '@':
                    cvars = max (cvars, int (attr.value[1:]))

    return Rule (id = id, text = text, attrs = attrs, parts = parts, cvars = cvars, flags=flags)

def import_rules (file):
    ruleptr = 0
    with open (file) as stream:
        try:
            obj = yaml.load (stream)
            for s in obj:
                pats = obj[s]
                rules[s] = []
                for p in pats:
                    if 'pat' in p:
                        rule = parse_rule (p['pat'], ruleptr)
                        if rule != None:
                            rules[s].append (rule)
                            ruleptr += 1
        except yaml.YAMLError as exc:
            print (exc)
            exit (1)
