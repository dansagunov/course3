from pymorphy2 import MorphAnalyzer

morph = MorphAnalyzer ()
word_attrs = ['POS', 'number', 'case', 'gender', 'tense', 'person', 'transitivity', 'mood', 'aspect']

def parse_dict_word (text):
    result = []
    if text.find (' ') != -1:
        return []
    if text == 'и':
        return [{'pos': 'conj'}]
    if text == 'в':
        return [{'pos': 'prep'}]
    if text == 'с':
        return [{'pos': 'prep'}]
    if text == 'всем':
        result.append ({'normal_form': 'всё', 'pos': 'npro', 'gender': 'neut', 'person': '3per', 'number': 'plur', 'case': 'loct'})
    if text == 'один':
        result.append ({'normal_form': 'один', 'pos': 'advb'})
    if text == 'своих':
        result.append ({'normal_form': 'свой', 'pos': 'npro', 'number': 'plur', 'person': '3per', 'case': 'gent'})
    parsed = morph.parse (text.lower ())
    for i in range (len (parsed)):
        t = parsed[i].tag
        cur = {'normal_form': parsed[i].normal_form}
        if getattr (t, 'POS') == None:
            continue
        for attr in word_attrs:
            if getattr (t, attr) != None:
                cur[attr.lower ()] = getattr (t, attr).lower ()
        result.append (cur)
    return result
